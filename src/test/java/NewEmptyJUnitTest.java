/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */

import java.security.NoSuchAlgorithmException;
import logic.BruteForceHashCrack;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

/**
 *
 * @author manel
 */
public class NewEmptyJUnitTest {
    
    public NewEmptyJUnitTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

   
    /***
     * Crack resultat correcte a les 1679616 iteracions
     * 
     * @throws NoSuchAlgorithmException 
     */
    @Test
    public void crackHash1() throws NoSuchAlgorithmException {
        
        BruteForceHashCrack b1 = new BruteForceHashCrack("abcdefghijklmnopqrstuvwxyz0123456789", "4d186321c1a7f0f354b297e8914ab240",4);
        
        b1.run();
        
        assertEquals("hola", b1.getResultString());
        assertEquals(1679616, b1.getIterations());
        
        System.out.println(b1.getIterations());
     
    }
    
    /***
     * Arriba al final sense trobar cap resultat. 
     * 
     * @throws NoSuchAlgorithmException 
     */
    @Test
    public void crackHash2() throws NoSuchAlgorithmException {
        
        BruteForceHashCrack b1 = new BruteForceHashCrack("0123456789", "4d186321c1a7f0f354b297e8214ab240",4);
        
        b1.run();
        
        assertEquals(null, b1.getResultString());
        assertEquals(10000, b1.getIterations());
     
    }
}
