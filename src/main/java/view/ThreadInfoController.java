/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package view;

import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import logic.BruteForceHashCrack;

/**
 * FXML Controller class
 *
 * @author manel
 */
public class ThreadInfoController implements Initializable {

    @FXML
    private ProgressBar progressBar;
    @FXML
    private Label lbl_pid;
    @FXML
    public Label lbl_hash;
    @FXML
    private Label lbl_timeElapsed;
    @FXML
    private Label lbl_iterations;
    @FXML
    private Label lbl_result;
    
    
    //tasca
    private BruteForceHashCrack tasca;
    
    private Integer longitud;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }

    @FXML
    private void onAction_btn_Cancel(ActionEvent event) {
        
        this.tasca.endTask();
    }
    
    public void setTasca(BruteForceHashCrack tasca) {
        this.tasca = tasca;
    }

    public BruteForceHashCrack getTasca() {
        return tasca;
    }
    
    public Integer getLongitud() {
        return this.longitud;
    }

    public void setLongitud(Integer longitud) {
        this.longitud = longitud;
    }
    
    /***
     * Realitza l'execució de la tasca
     */
    public void runTask()
    {
        try {
            //es crea la instancia de la tasca
            tasca = new BruteForceHashCrack("abcdefghijklmnopqrstuvwxyz0123456789", lbl_hash.getText(), longitud);
            
            //actualitza visualment el component
            updateVisual();
            
            // s'inicia
            this.tasca.run();
            
            //actualitza visualment el component
            updateVisual();
        } catch (NoSuchAlgorithmException ex) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText(ex.toString());
            alert.showAndWait();
        }
    }
    
    /***
    * Actualitza la part visual del ThreadInfoController
    */
    private void updateVisual()
    {
        lbl_pid.setText(Long.toString(Thread.currentThread().getId()));
        lbl_timeElapsed.setText(Long.toString(tasca.getTimeElapsed()));
        lbl_iterations.setText(Long.toString(tasca.getIterations()));
        lbl_result.setText(tasca.getResultString()==null?"----":tasca.getResultString());
        progressBar.setProgress(tasca.getProgress());
    }
}
