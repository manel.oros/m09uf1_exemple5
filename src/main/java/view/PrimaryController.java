/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package view;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;

/**
 * FXML Controller class
 *
 * @author manel
 */
public class PrimaryController implements Initializable {
    
    ThreadInfoController controller;
    
    Integer longitudParaula;

    @FXML
    private TextField txt_hash_to_break;
    
    @FXML
    private Button btn_crack;
    
    @FXML
    private FlowPane flow_Pane;
    
    @FXML
    private Label label1;
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        longitudParaula = 4;
        
        try {
            label1.setText("MD5 a trencar? (longitud de paraula: " + longitudParaula + ")");
            txt_hash_to_break.setText("4d186321c1a7f0f354b297e8914ab240");
            FXMLLoader loader = new FXMLLoader(getClass().getResource("threadInfo.fxml"));
            //just en aquest punt s'executa el metode initialize del ThreadInfoController
            flow_Pane.getChildren().add(loader.load());
            controller = loader.getController();
            controller.setLongitud(longitudParaula);
            
        } catch (IOException ex) {
            Logger.getLogger(PrimaryController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    

    @FXML
    private void onAction_btnCrack(ActionEvent event) {
        
        //verifica que l'entrada tingui forma de hash
        if (this.txt_hash_to_break.getText().trim().matches("^[0-9a-fA-F]{32}$"))
        {
            controller.lbl_hash.setText(this.txt_hash_to_break.getText());   
            controller.runTask();
        }
    }
    
}
